<?php if ( premmerce_saleszone_fs()->is__premium_only() ): ?>
    <?php if(premmerce_option('footer-before')) :?>
        <div class="page__before-footer">
            <div class="page__container">
                <?php echo do_shortcode(premmerce_option('footer-before')); ?>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>

</div><!-- .page__content -->

</div><!-- .page__wrapper -->

<!-- Footer -->
<footer class="page__fgroup">

    <?php if ( premmerce_saleszone_fs()->is__premium_only() ): ?>
        <?php if (is_front_page() && get_theme_mod('homepage-seo-text') && apply_filters('premmerce-homepage-footer-seo-text', true)) :?>
                <div class="page__container">
                    <div class="page__seo-text">
                        <div class="typo typo--seo">
                            <?php echo wp_kses_post(prepend_attachment(premmerce_option('homepage-seo-text'))); ?>
                        </div>
                    </div>
                </div>
        <?php endif;?>
    <?php endif;?>

    <?php do_action('premmerce_before_footer_1'); ?>

    <?php if(premmerce_option('footer_1')) :?>
    <div class="page__footer">
        <div class="page__container">
            <div class="footer">
                <?php $footer_columns = premmerce_option('footer_columns'); ?>
                <div class="footer__row footer__row--columns-<?php echo esc_attr($footer_columns); ?>">
                    <?php for ($i = 1; $i <= $footer_columns; $i++): ?>
                        <?php if (is_active_sidebar('footer_'. $i)): ?>
                            <div class="footer__col">
                                <?php dynamic_sidebar('footer_'. $i); ?>
                            </div>
                        <?php endif; ?>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if ( premmerce_saleszone_fs()->is__premium_only() ): ?>
        <?php do_action('premmerce_before_footer_2'); ?>

        <?php if(premmerce_option('footer_2')) :?>
        <div class="page__basement">
            <div class="page__container">
                <div class="basement">
                    <div class="row row--ib row--ib-mid">
                        <div class="col-xs-12 col-sm-12 col--align-left-sm col--spacer-xs">
                            <div class="basement__wrap">
                              <?php echo do_shortcode(premmerce_option('footer-copyright')); ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    <?php endif; ?>

</footer>

</div><!-- /.page__*-layout -->
</div><!-- /.page__body -->

<!-- Mobile slide frame -->
<div class="page__mobile" data-page-pushy-mobile>
    <?php get_template_part('parts/mobile-menu/mobile', 'frame'); ?>
</div>

<!-- Site background overlay when mobile menu is open -->
<div class="page__overlay hidden" data-page-pushy-overlay></div>

<?php wp_footer() ?>
</body>
</html>