<?php get_header(); ?>

<?php if(is_front_page()): ?>
    <div class="content__container">
        <div class="mg1-start-page">
            <div class="mg1-start-page__sidebar hidden-xs hidden-sm">

                <div class="mg1-start-page__sidebar-item">
                    <?php

                    /**
                     * @hooked premmerce_render_catalog_btn_menu - 10
                     */
                    do_action('premmerce_catalog_btn_menu');

                    ?>
                </div>

                <?php if (is_active_sidebar('homepage_widgets')) : ?>
                    <div class="mg1-start-page__sidebar-item">
                        <?php dynamic_sidebar('homepage_widgets'); ?>
                    </div>
                <?php endif; ?>

            </div>
            <?php if (have_posts()): the_post(); ?>
            <div class="mg1-start-page__content">
                <?php the_content(); ?>
            </div>
            <?php endif ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
<?php else: ?>
    <div class="content">
        <div class="content__container">

            <?php if (have_posts()): the_post(); ?>
                <div class="content__header">
                    <h1 class="content__title"><?php the_title(); ?></h1>
                </div>
                <div class="content__row">
                    <div class="typo">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endif ?>
            <?php wp_reset_postdata(); ?>

            <?php if(comments_open()) :?>
                <div class="content__row">
                    <?php comments_template(); ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>