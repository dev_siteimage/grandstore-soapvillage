<?php
/**
 * Setting for excerpt in premmerce recent post widget
 */

add_filter('excerpt_length', function(){
    return 40;
}, 30);

add_filter('premmerce_catalog_btn_drop', function(){
    return !is_front_page();
}, 15);

add_filter('premmerce-homepage-footer-seo-text', function(){
    return false;
});

add_filter('premmerce-catalog-carousel-frame-block-class', function(){
    return 'catalog-map';
});
//
add_filter('premmerce-catalog-carousel-item-block-class', function(){
    return 'catalog-map-item';
});


/**
 * Replace parent theme svg icons
 */
$replaced_icons = [
    'heart',
    'star',
    'compare',
    'heart-thin',
    'cart-thin',
    'arrow-big-right',
    'arrow-big-left'
];

foreach ($replaced_icons as $icon_name){
    add_filter('premmerce-svg-icon--'. $icon_name, function ($data) use ($icon_name){
        return [
            'html' => child_theme_get_svg($icon_name, $data['class']),
            'class' => $data['class']
        ];
    });
}

add_filter('premmerce-svg-icon--new', function ($data){
    return [
        'html' => ''
    ];
});

add_filter('premmerce-svg-icon--delete', function ($data){
    return [
        'html' => 'x'
    ];
});

add_filter('premmerce-kit-slider-attrs', function ($attr){
    return '';
});


/**
 * Checkout
 */

add_filter('premmerce-checkout-filed-class', function (){
    return 'form__field';
});

/**
 * Wihslit and checkout button type
 */
add_filter('premmerce_comparison_catalog_button_type', 'premmerce_filter_action_buttons_type');
add_filter('premmerce_comparison_catalog_snippet_button_type', 'premmerce_filter_action_buttons_type');
add_filter('premmerce_comparison_product_button_type', 'premmerce_filter_action_buttons_type');

add_filter('premmerce_wishlist_catalog_button_type', 'premmerce_filter_action_buttons_type');
add_filter('premmerce_wishlist_catalog_snippet_button_type', 'premmerce_filter_action_buttons_type');
add_filter('premmerce_wishlist_product_button_type', 'premmerce_filter_action_buttons_type');

function premmerce_filter_action_buttons_type($type){
    return 'button';
}

add_filter('gettext', function($text) {
    if( $text == 'Action' ) {
       return 'Акция';
   }
   else return $text;
});


add_filter('gettext', function($text) {
    if( $text == 'Hit' ) {
       return 'Хит';
   }
   else return $text;
});

add_filter('gettext', function($text) {
    if( $text == 'New product' ) {
       return 'Новинка';
   }
   else return $text;
});

add_filter('gettext', function($text) {
    if( $text == 'Buy' ) {
       return 'Купить';
   }
   else return $text;
});


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {

    unset($fields['billing']['billing_state']); 
    unset($fields['billing']['billing_postcode']);

    $fields['billing']['billing_city']['label'] = 'Город';
    return $fields;
}

// Making fields optional
add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );
 
// Our intercepted function - $ fields goes through the filter
function custom_override_default_address_fields( $address_fields ) {
    $address_fields['address_1']['required'] = false;
    $address_fields['postcode']['required'] = false; 
    $address_fields['city']['required'] = false; 
    
    return $address_fields;
}