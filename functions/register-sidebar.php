<?php
add_action('widgets_init', function () {

    register_sidebar([
        'name' => 'Front Page widgets',
        'id' => 'homepage_widgets',
        'description' => 'Widgets bar on Front page',
        'before_widget' => '<div class="mg1-start-page__sidebar-item %2$s" id="%1$s"><div class="widget-sidebar">',
        'after_widget' => '</div></div>',
        'before_title' => '<div class="widget-sidebar__header"><h2 class="widget-sidebar__title">',
        'after_title' => '</h2></div>'
    ]);

});
