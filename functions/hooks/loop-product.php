<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

remove_action('premmerce_loop_product_cut_after', 'woocommerce_template_loop_add_to_cart', 20);
add_action('premmerce_loop_product_cut_action_row', 'woocommerce_template_loop_add_to_cart', 1);