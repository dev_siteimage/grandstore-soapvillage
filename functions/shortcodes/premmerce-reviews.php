<?php

add_shortcode('premmerce_reviews', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'carousel',
        'limit'      => '20',
        'orderby'    => 'name',
        'order'      => 'ASC',
        'columns'    => '4',
        'hide_empty' => 1,
        'parent'     => '',
        'ids'        => '',
    ), $atts, 'premmerce_reviews' );


    $ids        = array_filter( array_map( 'trim', explode( ',', $atts['ids'] ) ) );
    $hide_empty = ( true === $atts['hide_empty'] || 'true' === $atts['hide_empty'] || 1 === $atts['hide_empty'] || '1' === $atts['hide_empty'] ) ? 1 : 0;

    // Get terms and workaround WP bug with parents/pad counts.
    $args = array(
        'orderby'    => $atts['orderby'],
        'order'      => $atts['order'],
        'hide_empty' => $hide_empty,
        'include'    => $ids,
        'pad_counts' => true,
        'child_of'   => $atts['parent'],
		'hierarchical' => false,
		'number'   => (int)$atts['limit'],
    );


    $product_categories = get_terms( 'product_cat', $args );

    $columns = absint( $atts['columns'] );

    wc_set_loop_prop( 'columns', $columns );

    ob_start();

    wc_get_template('../parts/shortcode-templates/premmerce-reviews/' . $atts['template'] . '.php',[
        'attributes' => $atts,
        'product_categories' => $product_categories
    ]);

    return ob_get_clean();
});