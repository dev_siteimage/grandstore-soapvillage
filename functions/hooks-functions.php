<?php


if (!function_exists('premmerce_render_checkout_page_heading')) {
    function premmerce_render_checkout_page_heading()
    {
        ob_start()
        ?>

        <div class="content__header">
            <h1 class="content__title"><?php the_title(); ?></h1>
        </div>

        <?php
        echo ob_get_clean();
    }
}


if(!function_exists('premmerce_render_oneclickorder_product_button')){
    function premmerce_render_oneclickorder_product_button(){

        global $product;

        $addRoute = site_url('wp-json/premmerce/oneclickorder/popup');

        ?>

        <div class="pc-product-action">

            <?php wc_get_template('premmerce-woocommerce-buy-now/oneclickorder-product-btn.php', array(
                'popupUrl'    => wp_nonce_url($addRoute,'wp_rest'),
                'productId' => $product->get_id(),
            )) ?>
        </div>

        <?php
    }
}


 /**
* Add label 'New Product'
*/

if(!function_exists('product_labels_add_new')){
    function product_labels_add_new(){
        global $product; ?>
 
        <?php
        $terms = get_the_terms($product->get_id(), 'product_tag');
        if (!empty($terms)):
            foreach ($terms as $term):
                if (get_term_field('slug', $term) == 'new-product'):
                    ?>
                    <div class="product-labels__item product-labels__item--new-product">
                        <span class="product-labels__text"><?php esc_html_e('New product', 'kabanosy') ?></span>
                    </div>
                <?php
                endif;
            endforeach;
        endif;
    }
 }

 if (!function_exists('premmerce_eviews_register')) {
    function premmerce_eviews_register()
    {
        ?>

            <?php get_template_part('shortcodes/premmerce-reviews.php');?>

        <?php
    }
}