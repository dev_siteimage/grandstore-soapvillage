<?php

add_action("wp_enqueue_scripts", function (){

    $uri = get_stylesheet_directory_uri();
    $theme = wp_get_theme();
    $version = $theme->get('Version');

    wp_enqueue_style("grandstore-styles", $uri . "/public/css/styles.min.css", ['saleszone-styles'], $version);
});