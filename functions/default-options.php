<?php

/**
 * Set theme default options
 */
add_filter('saleszone_default_options', function($default_options){

    $theme_options = [
        'content_width' => 1530,
        'header_layout' => 'layout_3',
        'footer_columns'   => '5',
        'footer_2'      => false,
        'category-product-flipper' => false,
        'category-product-add-to-cart-btn' => 'only-button',
        'category-product-short-description' => false,
        'product-tabs-type' => 'tabs',
        'add-to-cart-icon' => 'shopping-cart',
        'category-btn-add-to-cart-icon' => 1,
        'product-btn-add-to-cart-icon' => 1,
        'header-phone-title'     => '',
        'header-phone'     => "0 800 123-45-67 \n 0 800 123-45-67",
        'header-phone-icon-style'     => 'phone-fill',
        'header-phone-display-type' => 'list-horizontal',
        'header-phone-icon-size' => '13',
        'product-main-attributes' => 1,


    ];

    return array_merge($default_options,$theme_options);
});

/**
 * Override theme backgounds
 */
add_filter('premmerce_default_options_background', function($background_settings){

    $theme_background_settings = [];

    foreach ($background_settings as $background_setting){
        switch ($background_setting['name']){
            case 'body_background_setting':
                $background_setting['default']['background-color'] = '#ffffff';
                break;
            case 'footer_background_setting':
                $background_setting['default']['background-color'] = '#4d60a0';
                break;
            case 'header_background_setting':
                $background_setting['default']['background-color'] = '#ffffff';
                break;
        }
        $theme_background_settings[] = $background_setting;
    }
    return $theme_background_settings;
});

/**
 * Override theme colors
 */

if (!function_exists('premmerce_default_options_css_variables')) {
    function premmerce_default_options_css_variables($type = false)
    {
        $css_variables = [
            // FOOTER
            [
            'name' => 'footer-aside-bg-color', // original
            'data' => [
                    'default' => '#465892',
                    'type'  => 'footer',
                    'label' => __('Side panel background color','saleszone')
                ]
            ],
            [
                'name' => 'footer-text-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'footer',
                    'label' => __('Text color','saleszone')
                ]
            ],
            [
                'name' => 'footer-title-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'footer',
                    'label' => __('Heading color','saleszone')
                ]
            ],
            [
                'name' => 'footer-link-color',
                'data' => [
                    'default' => '#c8d5ff',
                    'type'  => 'footer',
                    'label' => __('Links color','saleszone')
                ]
            ],
            [
                'name' => 'footer-link-color-hover',
                'data' => [
                    'default' => '#c8d5ff',
                    'type'  => 'footer',
                    'label' => __('Links hover color','saleszone')
                ]
            ],
            [
                'name' => 'footer-border-color',
                'data' => [
                    'default' => '#7c94e5',
                    'type'  => 'footer',
                    'label' => __('Border color','saleszone')
                ]
            ],
            [
                'name' => 'footer-social-links-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'footer',
                    'label' => __('Social icons color','saleszone')
                ]
            ],
            [
                'name' => 'footer-social-links-color-hover',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'footer',
                    'label' => __('Social icons hover color','saleszone')
                ]
            ],

            //Basement
            [
                'name' => 'basement-text-color',
                'data' => [
                    'default' => '#c8d5ff',
                    'type'  => 'basement',
                    'label' => __('Text color','saleszone')
                ]
            ],
            [
                'name' => 'basement-link-color',
                'data' => [
                    'default' => '#c8d5ff',
                    'type'  => 'basement',
                    'label' => __('Link color','saleszone')
                ]
            ],
            [
                'name' => 'basement-link-color-hover',
                'data' => [
                    'default' => '#c8d5ff',
                    'type'  => 'basement',
                    'label' => __('Link hover color','saleszone')
                ]
            ],
            [
                'name' => 'basement-bg-color',
                'data' => [
                    'default' => '#4d60a0',
                    'type'  => 'basement',
                    'label' => __('Background color','saleszone')
                ]
            ],
            [
                'name' => 'basement-border-color',
                'data' => [
                    'default' => '#7c94e5',
                    'type'  => 'basement',
                    'label' => __('Border color','saleszone')
                ]
            ],

            //Headline
            [
                'name' => 'headline-text-color',
                'data' => [
                    'default' => '#999999',
                    'type'  => 'headline',
                    'label' => __('Text color','saleszone')

                ]
            ],
            [
                'name' => 'headline-link-color',
                'data' => [
                    'default' => '#4d60a0',
                    'type'  => 'headline',
                    'label' => __('Link color','saleszone')
                ]
            ],
            [
                'name' => 'headline-link-color-hover',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'headline',
                    'label' => __('Link hover color','saleszone')
                ]
            ],
            [
                'name' => 'headline-bg-color',
                'data' => [
                    'default' => '#f2f6f9',
                    'type'  => 'headline',
                    'label' => __('Background color','saleszone')
                ]
            ],
            [
                'name' => 'headline-border-top-color',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'headline',
                    'label' => __('Border top color','saleszone')
                ]
            ],
            [
                'name' => 'headline-border-color',
                'data' => [
                    'default' => '#eaeaea',
                    'type'  => 'headline',
                    'label' => __('Border color','saleszone')
                ]
            ],
            [
                'name' => 'headline-icon-color',
                'data' => [
                    'default' => '#fd9f3b',
                    'type'  => 'headline',
                    'label' => __('Icons color','saleszone')
                ]
            ],
            [
                'name' => 'headline-panel-item-bg',
                'data' => [
                    'default' => '#f8f8f8',
                    'type'  => 'headline',
                    'label' => __('Headline panel item background','saleszone')
                ]
            ],
            [
                'name' => 'headline-panel-item-bg-hover',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'headline',
                    'label' => __('Headline panel item background hover','saleszone')
                ]
            ],
            //Header phone
            [
                'name' => 'header-phone-color',
                'data' => [
                    'default' => '#000000',
                    'type'  => 'header-phone',
                    'label' => __('Phone number color','saleszone')
                ]
            ],
            [
                'name' => 'header-phone-icon-color',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'header-phone',
                    'label' => __('Phone icon color','saleszone')
                ]
            ],

            //Header
            [
                'name' => 'header-border-bottom-color',
                'data' => [
                    'default' => 'rgba(0,0,0, 0)',
                    'type'  => 'header-main',
                    'label' => __('Border bottom color','saleszone')
                ]
            ],
            [
                'name' => 'header-icon-color',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'header-main',
                    'label' => __('Icons color','saleszone')
                ]
            ],
            [
                'name' => 'header-text-color',
                'data' => [
                    'default' => '#666666',
                    'type'  => 'header-main',
                    'label' => __('Text color','saleszone')
                ]
            ],
            [
                'name' => 'header-link-color',
                'data' => [
                    'default' => '#4d60a0',
                    'type'  => 'header-main',
                    'label' => __('Link color','saleszone')
                ]
            ],
            [
                'name' => 'header-link-color-hover',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'header-main',
                    'label' => __('Link hover color','saleszone')
                ]
            ],
            [
                'name' => 'header-search-btn-bg',
                'data' => [
                    'default' => '#fd9f3b',
                    'type'  => 'header-main',
                    'label' => __('Search button background color','saleszone')
                ]
            ],
            [
                'name' => 'header-bottom-bg-color',
                'data' => [
                    'default' => '#4d60a0',
                    'type'  => 'header-bottom',
                    'label' => __('Header bottom background color','saleszone')
                ]
            ],

            //Main navigation
            [
                'name' => 'main-nav-bg-color',
                'data' => [
                    'default' => '#4d60a0',
                    'type'  => 'main-navigation',
                    'label' => __('Background color','saleszone')
                ]
            ],
            [
                'name' => 'main-nav-bg-color-hover',
                'data' => [
                    'default' => '#586caf',
                    'type'  => 'main-navigation',
                    'label' => __('Background hover color','saleszone')
                ]
            ],
            [
                'name' => 'main-nav-bg-color-active',
                'data' => [
                    'default' => '#586caf',
                    'type'  => 'main-navigation',
                    'label' => __('Background active color','saleszone')
                ]
            ],
            [
                'name' => 'main-nav-text-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'main-navigation',
                    'label' => __('Text color','saleszone')
                ]
            ],
            [
                'name' => 'main-nav-text-color-hover',
                'data' => [
                    'default' => '#fd9f3b',
                    'type'  => 'main-navigation',
                    'label' => __('Text hover color','saleszone')
                ]
            ],
            [
                'name' => 'header-catalog-btn',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'main-navigation',
                    'label' => __('Catalog button color','saleszone')
                ]
            ],

            //Off-Canvas Menu
            [
                'name' => 'off-canvas-hamburger-color',
                'data' => [
                    'default' => '#000000',
                    'type'  => 'off-canvas',
                    'label' => __('Hamburger icon color','saleszone')
                ]
            ],
            [
                'name' => 'off-canvas-bg-color',
                'data' => [
                    'default' => '#4d60a0',
                    'type'  => 'off-canvas',
                    'label' => __('Menu background color','saleszone')
                ]
            ],
            [
                'name' => 'off-canvas-border-color',
                'data' => [
                    'default' => '#586caf',
                    'type'  => 'off-canvas',
                    'label' => __('Menu item border color','saleszone')
                ]
            ],
            [
                'name' => 'off-canvas-link-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'off-canvas',
                    'label' => __('Menu item text color','saleszone')
                ]
            ],
            [
                'name' => 'off-canvas-link-hover-bg',
                'data' => [
                    'default' => '#fd9f3b',
                    'type'  => 'off-canvas',
                    'label' => __('Menu item hover color','saleszone')
                ]
            ],
            [
                'name' => 'off-canvas-heading-bg',
                'data' => [
                    'default' => '#fd9f3b',
                    'type'  => 'off-canvas',
                    'label' => __('Heading background color','saleszone')
                ]
            ],
            [
                'name' => 'off-canvas-heading-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'off-canvas',
                    'label' => __('Heading text color','saleszone')
                ]
            ],


            // Body links
            [
                'name' => 'base-main-link-color',
                'data' => [
                    'default' => '#4d60a0',
                    'type'  => 'links',
                    'label' => __('Theme link color','saleszone')
                ]
            ],
            [
                'name' => 'base-main-link-hover-color',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'links',
                    'label' => __('Theme link hover color','saleszone')
                ]
            ],
            // Body buttons
            [
                'name' => 'btn-primary-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'buttons',
                    'label' => __('Primary button color','saleszone')
                ]
            ],
            [
                'name' => 'btn-primary-hover-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'buttons',
                    'label' => __('Primary button hover color','saleszone')
                ]
            ],
            [
                'name' => 'btn-primary-bg',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'buttons',
                    'label' => __('Primary button background','saleszone')
                ]
            ],
            [
                'name' => 'btn-primary-hover-bg',
                'data' => [
                    'default' => '#567117',
                    'type'  => 'buttons',
                    'label' => __('Primary button hover background','saleszone')
                ]
            ],
            [
                'name' => 'btn-primary-border',
                'data' => [
                    'default' => '#6c8f1c   ',
                    'type'  => 'buttons',
                    'label' => __('Primary button border','saleszone')
                ]
            ],
            [
                'name' => 'btn-primary-hover-border',
                'data' => [
                    'default' => '#465c12',
                    'type'  => 'buttons',
                    'label' => __('Primary button hover border','saleszone')
                ]
            ],
            [
                'name' => 'btn-default-color',
                'data' => [
                    'default' => '#333333',
                    'type'  => 'buttons',
                    'label' => __('Default button color','saleszone')
                ]
            ],
            [
                'name' => 'btn-default-hover-color',
                'data' => [
                    'default' => '#333333',
                    'type'  => 'buttons',
                    'label' => __('Default button hover color','saleszone')
                ]
            ],
            [
                'name' => 'btn-default-bg',
                'data' => [
                    'default' => '#f8f8f8',
                    'type'  => 'buttons',
                    'label' => __('Default button background','saleszone')
                ]
            ],
            [
                'name' => 'btn-default-hover-bg',
                'data' => [
                    'default' => '#dfdfdf',
                    'type'  => 'buttons',
                    'label' => __('Default button hover background','saleszone')
                ]
            ],
            [
                'name' => 'btn-default-border',
                'data' => [
                    'default' => '#eaeaea',
                    'type'  => 'buttons',
                    'label' => __('Default button border','saleszone')
                ]
            ],
            [
                'name' => 'btn-default-hover-border',
                'data' => [
                    'default' => '#cbcbcb',
                    'type'  => 'buttons',
                    'label' => __('Default button hover border','saleszone')
                ]
            ],

            // Panels
            [
                'name' => 'panels-bg-color',
                'data' => [
                    'default' => '#f8f8f8',
                    'type'  => 'panels',
                    'label' => __('Panels background color','saleszone')
                ]
            ],
            [
                'name' => 'panels-heading-color',
                'data' => [
                    'default' => '#000000',
                    'type'  => 'panels',
                    'label' => __('Panels heading color','saleszone')
                ]
            ],
            [
                'name' => 'panels-text-color',
                'data' => [
                    'default' => '#666666',
                    'type'  => 'panels',
                    'label' => __('Panel text color','saleszone')
                ]
            ],

            // Message
            [
                'name' => 'message-success-color',
                'data' => [
                    'default' => '#dff0d8',
                    'type'  => 'message',
                    'label' => __('Message success color','saleszone')
                ]
            ],
            [
                'name' => 'message-success-border',
                'data' => [
                    'default' => '#a3d48e',
                    'type'  => 'message',
                    'label' => __('Message success border color','saleszone')
                ]
            ],
            [
                'name' => 'message-error-color',
                'data' => [
                    'default' => '#ffefe8',
                    'type'  => 'message',
                    'label' => __('Message error color','saleszone')
                ]
            ],
            [
                'name' => 'message-error-border',
                'data' => [
                    'default' => '#e89b88',
                    'type'  => 'message',
                    'label' => __('Message error border color','saleszone')
                ]
            ],
            [
                'name' => 'message-info-color',
                'data' => [
                    'default' => '#fcf8e3',
                    'type'  => 'message',
                    'label' => __('Message info color','saleszone')
                ]
            ],
            [
                'name' => 'message-info-border',
                'data' => [
                    'default' => '#efe4ae',
                    'type'  => 'message',
                    'label' => __('Message info border color','saleszone')
                ]
            ],

            // Other
            [
                'name' => 'theme-main-color',
                'data' => [
                    'default' => '#769c1f',
                    'type'  => 'other',
                    'label' => __('Theme main color','saleszone')
                ]
            ],
            [
                'name' => 'base-font-color',
                'data' => [
                    'default' => '#666666',
                    'type'  => 'other',
                    'label' => __('Base site font color','saleszone')
                ]
            ],
            [
                'name' => 'base-font-color-secondary',
                'data' => [
                    'default' => '#9aa1ab',
                    'type'  => 'other',
                    'label' => __('Secondary font color','saleszone')
                ]
            ],
            [
                'name' => 'base-border-color',
                'data' => [
                    'default' => '#eaeaea',
                    'type'  => 'other',
                    'label' => __('Base border color','saleszone')
                ]
            ],
            [
                'name' => 'star-rating-color',
                'data' => [
                    'default' => '#ffb300',
                    'type'  => 'other',
                    'label' => __('Star rating color','saleszone')
                ]
            ],
            [
                'name' => 'star-empty-color',
                'data' => [
                    'default' => '#ffffff',
                    'type'  => 'other',
                    'label' => __('Star rating empty color','saleszone')
                ]
            ],

            // Form controls
            [
                'name' => 'form-conrols-border-color',
                'data' => [
                    'default' => '#c8cfd9',
                    'type'  => 'form-controls',
                    'label' => __('Form controls border color','saleszone')
                ]
            ],
            [
                'name' => 'form-conrols-box-shadow',
                'data' => [
                    'default' => 'rgba(102,175,233,.6)',
                    'type'  => 'form-controls',
                    'label' => __('Form controls focused border color','saleszone')
                ]
            ],

            // Shop
            [
                'name' => 'product-price-color',
                'data' => [
                    'default' => '#000000',
                    'type'  => 'shop',
                    'label' => 'Product price color'
                ]
            ],
            [
                'name' => 'product-old-price-color',
                'data' => [
                    'default' => '#999999',
                    'type'  => 'shop',
                    'label' => __('Product old price color','saleszone')
                ]
            ],

            // hidden for user
            [
                'name' => 'base-box-shadow-color',
                'data' => [
                    'default' => 'rgba(0, 0, 0, 0.15)',
                    'type'  => 'hidden',
                    'label' => __('Product old price color','saleszone')
                ]
            ],
        ];

        if($type != false){
            return array_filter($css_variables, function($option) use ($type) {
                return $option['data']['type'] == $type;
            });
        } else {
            return $css_variables;
        }
    }
}