<?php

add_action('init', function(){

    remove_action('premmerce_catalog_btn_menu','premmerce_render_catalog_btn_menu'. 10);

    /**
     * Loop product
     */
    require get_stylesheet_directory() . "/functions/hooks/loop-product.php";
    require get_stylesheet_directory() . "/functions/theme-customizer.php";


    /**
     * Page product
     */

    remove_action('premmerce_add_to_cart_button','woocommerce_template_single_price', 5);
    add_action('woocommerce_before_add_to_cart_form', 'woocommerce_template_single_price', 5);

    /**
     * Compare
     */
    remove_action('woocommerce_after_add_to_cart_button', 'premmerce_render_compare_product_button', 10);
    add_action('premmerce_add_to_cart_button', 'premmerce_render_compare_product_button', 15);

    /**
     * Wishlist
     */
    remove_action('woocommerce_after_add_to_cart_button','premmerce_render_wishlist_product_button', 10);
    add_action('premmerce_add_to_cart_button', 'premmerce_render_wishlist_product_button', 15);


    add_action('woocommerce_checkout_before_customer_details', 'premmerce_render_checkout_page_heading', 5);

    /**
     * Add labels 'New Product'
    */
    add_action('premmerce_product_labels', 'product_labels_add_new');
});

// add_action( 'init', 'premmerce_eviews_register');