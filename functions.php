<?php

/**
 * Insert custom and third party styles and scripts into index page
 */

require dirname(__FILE__) . "/functions/assets.php";
require dirname(__FILE__) . "/functions/utilities.php";

require dirname(__FILE__) . "/functions/default-options.php";
require dirname(__FILE__) . "/functions/hooks-functions.php";
require dirname(__FILE__) . "/functions/hooks.php";
require dirname(__FILE__) . "/functions/filters.php";
require dirname(__FILE__) . "/functions/register-sidebar.php";

if (!function_exists('soap_render_variants')) {

    /**
     * @param $product
     */
    function soap_render_variants($product)
    {
        $available_variations = $product->get_available_variations();
        foreach ($available_variations as $variation) {
            foreach ($variation['attributes'] as $attributeName => $attribute) {
                $attributeName = str_replace("attribute_", "", $attributeName);
                if ($product && taxonomy_exists($attributeName)) {
                    // Get terms if this is a taxonomy - ordered. We need the names too.
                    $terms = wc_get_product_terms(
                        $product->get_id(),
                        $attributeName,
                        array(
                            'fields' => 'all',
                        )
                    );
                    foreach ($terms as $term) {
                        if (in_array($term->slug, [$attribute], true)) {
                            ?>
                            <label class="pc-product-purchase__variation-label" style="display: block">
                                <div class="pc-product-purchase__variation-label-item">
                                    <span class="pc-product-purchase__variation-label-icon">
                                        <input form="add-to-cart-form-<?= $product->get_id() ?>" type="radio" name="test">
                                        <span class="icon-check"></span>
                                        <span><?= $term->name ?></span>
                                    </span>
                                    <span class="pc-product-purchase__variation-label-divider"></span>
                                    <span class="pc-product-purchase__variation-label-price"><?= wc_price($variation['display_price']) ?></span>
                                </div>
                            </label>
                            <?php
                        }
                    }
                }
            }
        }
    }
}