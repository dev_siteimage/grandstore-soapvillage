<?php

$frame_block_name = esc_attr(apply_filters('premmerce-catalog-carousel-frame-block-class','pc-section-primary'));
$block_name = esc_attr(apply_filters('premmerce-catalog-carousel-item-block-class','catalog-section'));

?>

<section class="<?php echo $frame_block_name; ?>" data-slider="section-primary">
    <?php if (!empty($attributes['title'])): ?>
        <div class="<?php echo $frame_block_name; ?>__title"><?php echo $attributes['title']; ?></div>
    <?php endif; ?>
    <div class="<?php echo $frame_block_name; ?>__inner">
        <nav>
            <ul class="row row--ib row--ib row--vindent-m" data-slider-slides="1,2,3,<?php echo $attributes['columns']; ?>">

                <?php foreach ($product_categories as $category): ?>

                    <?php $loc_image_id = get_term_meta($category->term_id, 'thumbnail_id', true); ?>

                    <li class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                        <a class="<?php echo $block_name; ?>" href="<?php echo get_term_link($category, 'product_cat'); ?>">
                            <div class="<?php echo $block_name; ?>__image">
                                <img class="<?php echo $block_name; ?>__img" src="<?php echo wp_get_attachment_image_src($loc_image_id, 'thumbnail', true)[0]; ?>"
                                     alt="<?php echo esc_attr(premmerce_get_img_alt($loc_image_id, $category->name)); ?>"
                                >
                            </div>
                            <div class="<?php echo $block_name; ?>__caption">
                                <?php echo $category->name; ?>
                            </div>
                        </a>
                    </li>

                <?php endforeach; ?>

            </ul>
        </nav>
    </div>
    <div class="<?php echo $frame_block_name; ?>__arrow <?php echo $frame_block_name; ?>__arrow--left hidden" data-slider-arrow-left>
        <?php premmerce_the_svg('arrow-big-left') ?>
    </div>
    <div class="<?php echo $frame_block_name; ?>__arrow <?php echo $frame_block_name; ?>__arrow--right hidden" data-slider-arrow-right>
        <?php premmerce_the_svg('arrow-big-right') ?>
    </div>
</section>